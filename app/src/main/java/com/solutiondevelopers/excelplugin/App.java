package com.solutiondevelopers.excelplugin;

import android.app.Application;
import android.os.Environment;

import java.io.File;

class App extends Application {
    public static boolean isBackupRunning, isBackupStoppedDueToOtherReason;
    public static final String APP_FOLDER_NAME = "ImportSample";
    public static final String APP_FOLDER_PATH = Environment.getDownloadCacheDirectory() + File.separator + APP_FOLDER_NAME;
    public static final String APP_FOLDER_PATH_BACKUP = APP_FOLDER_PATH +  " Backup";

}
