package com.solutiondevelopers.excelplugin;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import static com.solutiondevelopers.excelplugin.App.APP_FOLDER_PATH_BACKUP;
import static com.solutiondevelopers.excelplugin.App.isBackupRunning;
import static com.solutiondevelopers.excelplugin.App.isBackupStoppedDueToOtherReason;

public class BackupService extends Service {

    private static final String TAG = "BackupService";

    private NotificationManager notificationManager;
    private Notification notification;
    private CountDownTimer countDownTimer;
    private Notification.Builder mBuilder;
    private NotificationCompat.Builder mBuilderCompat;
    private File f;


    @Override
    public void onCreate() {
        super.onCreate();

        //initialising notification compat builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBuilder = new Notification.Builder(getApplicationContext(), "backup_channel");
        } else {
            mBuilderCompat = new NotificationCompat.Builder(getApplicationContext(), "backup_channel");
            mBuilderCompat.setSound(null);
        }
        createForeGround();


        f = new File(APP_FOLDER_PATH_BACKUP);

        //Create new file
        if (f.mkdirs()){
            OutputStream fileOut;
            try {
                fileOut = new FileOutputStream(new File(f,"myFile.txt"));
            } catch (FileNotFoundException e) {
                Log.e(TAG, "onCreate: Could not create file ",e );
            }

        } else {
            //Stopping service if file not created, probably because storage permission revoked
            isBackupStoppedDueToOtherReason = true;
            Log.e(TAG, "onCreate: Could not create folder " );

        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        
        if (isBackupRunning) {
            Toast.makeText(this, "Backup running", Toast.LENGTH_SHORT).show();
            stopForeground(true);
            stopSelf();
            return START_NOT_STICKY;
        }

        isBackupRunning = true;

        //1 minute count down timer
        countDownTimer = new CountDownTimer(60 * 1000, 15000) {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.d(TAG, "onTick: millisUntilFinished : " + millisUntilFinished);

                        isBackupRunning = false;

                        stopForeground(true);
                        stopSelf();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            notification = mBuilder
                                    .setContentTitle(getString(R.string.backup_completed))
                                    .setContentText(getString(R.string.backup_complete_desc))
                                    .setAutoCancel(true)
                                    .setProgress(0, 0, false)
                                    .build();
                        }  else {
                            notification = mBuilderCompat
                                    .setContentTitle(getString(R.string.backup_completed))
                                    .setContentText(getString(R.string.backup_complete_desc))
                                    .setAutoCancel(true)
                                    .setProgress(0, 0, false)
                                    .build();
                        }
                        if (notificationManager != null) {
                            notificationManager.notify(1, notification);
                        }
                        countDownTimer.cancel();
                        Log.d(TAG, "onTick: Done with table backup");



            }

            @Override
            public void onFinish() {
                Log.d(TAG, "onFinish: countDownTimer");
                isBackupRunning = false;

                stopForeground(true);
                stopSelf();

                if (!isBackupStoppedDueToOtherReason) {
                    Log.d(TAG, "onTick: No storage permission, done with table backup");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notification = mBuilder
                                .setContentTitle(getString(R.string.backup_failed))
                                .setContentText(getString(R.string.backup_failed_info))
                                .build();
                    } else {
                        notification = new NotificationCompat.Builder(getApplicationContext(), "backup_channel")
                                .setContentTitle(getString(R.string.backup_failed))
                                .setContentText(getString(R.string.backup_failed_info))
                                .build();
                    }
                    notificationManager.notify(1, notification);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notification = mBuilder
                                .setContentTitle(getString(R.string.backup_could_not_start))
                                .setContentText(getString(R.string.permission_storage))
                                .build();
                    } else {
                        notification = new NotificationCompat.Builder(getApplicationContext(), "backup_channel")
                                .setContentTitle(getString(R.string.backup_could_not_start))
                                .setContentText(getString(R.string.permission_storage))
                                .build();
                    }
                    notificationManager.notify(1, notification);


                }
            }
        };
        countDownTimer.start();

        return START_STICKY;
    }


    private void createForeGround() {

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("backup_channel", getString(R.string.backup), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(getString(R.string.backup_intro));
            channel.setSound(null,null);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification = mBuilder
                    .setContentTitle(getString(R.string.running_backup))
                    .setContentText(getString(R.string.please_wait))
                    .setSmallIcon(android.R.drawable.stat_sys_download)
                    .build();
        } else {
            notification = mBuilderCompat
                    .setContentTitle(getString(R.string.running_backup))
                    .setContentText(getString(R.string.please_wait))
                    .setSmallIcon(android.R.drawable.stat_sys_download)
                    .setSound(null)
                    .build();
        }
        startForeground(1, notification);

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        isBackupRunning = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // TODO: 28-06-2019 To handle if no tables are made

}
