package com.solutiondevelopers.excelplugin;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import static com.solutiondevelopers.excelplugin.App.APP_FOLDER_PATH_BACKUP;
import static com.solutiondevelopers.excelplugin.App.isBackupStoppedDueToOtherReason;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startService(View view) {
//        this.startForegroundService(new Intent(this, BackupService.class));

        File f = new File(APP_FOLDER_PATH_BACKUP);

        //Create new file
        if (f.mkdir()){
            OutputStream fileOut;
            try {
                fileOut = new FileOutputStream(new File(f,"myFile.txt"));
            } catch (FileNotFoundException e) {
                Log.e("main activity", "onCreate: Could not create file ",e );
            }

        } else {
            //Stopping service if file not created, probably because storage permission revoked
            isBackupStoppedDueToOtherReason = true;
            Log.e("main activity", "onCreate: Could not create folder " );

        }

    }
}